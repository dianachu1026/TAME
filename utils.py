'''
Author: Jiawen
Email: dianachu1026@gmail.com
Date: 2021-07-27 17:05:23
LastEditors: Jiawen
LastEditTime: 2021-07-27 17:47:22
Description: None
'''
import os
import numpy as np
import pandas as pd
import torchvision
import torch
import torch.nn as nn
import random
from PIL import Image
from transformers import BertTokenizer, VisualBertModel, LxmertTokenizer, LxmertModel
from torch.utils.data import Dataset, DataLoader,TensorDataset


def setup_seed(seed):
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    torch.backends.cudnn.deterministic = True
    
def extract_list(race):
    result=[]
    for info in race:
        if 'race' in info.keys() and info['race'] is not None:
            result.append(info['race'])
        if 'gender' in info.keys() and info['gender'] is not None:
            result.append(info['gender'])
    if len(result)==0:
         result.append('unk')
    return result

def get_list(entity):
    result=[]
    for e in entity:
        if len(e)>0:
            result.extend(e)
        if len(result)==0:
            result.append('unk')
    return result

def load_data(cls_name, args):
    data_dir = args.data_dir
    train_path = data_dir + "/train.jsonl"
    test_path = data_dir + "/dev_seen.jsonl"
     
    train_samples_frame = pd.read_json(train_path, lines=True)
    test_samples_frame = pd.read_json(test_path, lines=True)
    
    zsl_test = test_samples_frame[test_samples_frame["cls"] == "non_hateful"]
    train_hate = train_samples_frame[train_samples_frame["cls"] == cls_name]
    
    train = train_samples_frame.drop(train_samples_frame[train_samples_frame["cls"] == cls_name].index)
    
    gzsl_test = test_samples_frame
    hate_test = test_samples_frame[test_samples_frame["cls"] == cls_name] #cla_name
    hate_test = hate_test.append(train_hate)
    
    # for normal setting, return "train_samples_frame", for zsl setting, return "train"
    return train, gzsl_test, zsl_test, hate_test

def prepare_data(data, args):
    img = data["img"]
    text = list(data["text"])
    clas = list(data["cls"])
    label = list(data["label"])
    label = torch.as_tensor(label, dtype=torch.long)
        
    learned_data = list(zip(img, text, clas, label))
            
    data_loader = DataLoader(
        learned_data,
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=4
    )
    return data_loader

class LXMERT(nn.Module):
    def __init__(self, lxrt_encoder):
        super(LXMERT, self).__init__()
        self.lxrt_encoder=lxrt_encoder
        
    def forward(self,feat,bbox,sent):
        x=self.lxrt_encoder(sent,(feat,bbox))
        return x

class Vil_Bert(nn.Module):
    def __init__(self, encoder):
        super(Vil_Bert, self).__init__()
        self.encoder=encoder
        
    def forward(self,token,mask,feat,vis_mask,bbox):
        pooled_output, vil_prediction, vil_logit, vil_binary_prediction, vision_prediction, vision_logit, linguisic_prediction, linguisic_logit=\
        self.encoder(token,
                     feat,
                     bbox,
                     attention_mask=mask,
                     image_attention_mask=vis_mask)
        return pooled_output

def get_model(args, model_name):
    if model_name == "visualbert":
        model = VisualBertModel.from_pretrained("uclanlp/visualbert-nlvr2-coco-pre").cuda()
        return model
    
    elif model_name == "lxmert":
#         model = LxmertModel.from_pretrained('unc-nlp/lxmert-base-uncased').cuda()
#         return model
        from lxrt.entry import LXRTEncoder
        from lxrt.modeling import BertLayerNorm, GeLU
        from pretrain.qa_answer_table import load_lxmert_qa
        lxrt_encoder=LXRTEncoder(args, args.LENGTH)
        model=LXMERT(lxrt_encoder).cuda()
        model_path=args.load_lxmert_qa
        load_lxmert_qa(model_path,model)
        return model
    
    elif model_name == "vilbert":
        from vilbert.vilbert import BertConfig
        from vilbert.vilbert import VILBertForVLTasks
        args_file=BertConfig.from_json_file(args.BERT_CONFIG)
        encoder= VILBertForVLTasks.from_pretrained(
#             args.PRE_BERT_TYPE,
            "./model-outputs/vilbert/pytorch_model_8.bin",
            args_file, 
            num_labels=1
        )
        encoder = encoder.cuda()
        return Vil_Bert(encoder)

class get_image_feature(nn.Module):
    def __init__(self, args):
        super(get_image_feature, self).__init__()
        self.baseline_name = args.baseline_name
        
    def forward(self, image):
        image_features = []
        visual_pos = []
        for i in range(len(image)):
            num = os.path.splitext(image[i])[0]
            file_path = "../hateful_memes/features/" + str(num) + ".npy"
            img_item = "../hateful_memes/" + str(num) + ".png"
            a = np.load(file_path ,encoding = "latin1", allow_pickle=True)
            data = a.item()
            if self.baseline_name == "visualbert":
                feature = data["features"].reshape(20,1024)
            else:
                feature = data["features"]
                
            boxes = data["boxes"]
            img = Image.open(img_item)
            im = np.array(img).astype(np.float32)
            im_shape = im.shape
            im_height = im_shape[0]
            im_width = im_shape[1]
            
            # bbox normalization
            for item in boxes:
                item[0] = item[0]/im_width
                item[1] = item[1]/im_height
                item[2] = item[2]/im_width
                item[3] = item[3]/im_height
                
            if self.baseline_name == "vilbert":
                area = im_height*im_width
                boxes = torch.Tensor(boxes)
#                 print(boxes)
#                 print(type(boxes))
#                 print(boxes.shape)
                cover_area=(boxes[:,2]-boxes[:,0])*(boxes[:,3]-boxes[:,1])/area
                cover_area = torch.Tensor(cover_area)
#                 print(cover_area.unsqueeze(1))
#                 print(type(cover_area))
#                 print(cover_area.shape)
                boxes = torch.cat((boxes, cover_area.unsqueeze(1)), dim=1)
                boxes = np.array(boxes)
                
            image_features.append(feature)
            visual_pos.append(boxes)
            
#             print(visual_pos)

        image_features = torch.Tensor(image_features).cuda()
        visual_pos = torch.Tensor(visual_pos).cuda()
        
        return image_features, visual_pos

class FeatureExtractor(nn.Module):
    def __init__(self, args, model, transformer):
        super(FeatureExtractor, self).__init__()
        self.args = args
        self.baseline_name = args.baseline_name
        self.get_image_feature = get_image_feature(args)
        self.model = model
        self.text_transform = transformer

    def forward(self, text, image):
        image_features, visual_pos = self.get_image_feature(image)
        token = self.text_transform(
            text,
            padding="max_length",
            max_length=32,       # normal: 32
            truncation=True,
            return_token_type_ids=True,
            return_attention_mask=True,
            add_special_tokens=True,
            return_tensors="pt"
        )
        token_mask = token.attention_mask.cuda()
        invalid = (torch.sum(image_features,dim = 2) == 0)
        vis_mask = 1 - invalid.int()
#         vis_mask = torch.ones(image_features.shape[:-1], dtype=torch.float).cuda()
        inputs = {}
        
        if self.baseline_name == "visualbert": 
            bz,text_len = token.token_type_ids.shape
            _, vis_len, _ = image_features.shape
            token_id = torch.ones(bz,text_len).long().cuda()
            vis_id = torch.ones(bz,vis_len).long().cuda()
            inputs.update({
                'attention_mask':token_mask,
                'input_ids':token.input_ids.cuda(),
                'visual_embeds':image_features,
                'visual_attention_mask':vis_mask,
                'token_type_ids':token_id,
                'visual_token_type_ids':vis_id
            })
            seq_repre=self.model(**inputs).last_hidden_state
            joint_repre=torch.sum(seq_repre,dim=1)

        elif self.baseline_name == "lxmert":
#             inputs.update({
#                 'input_ids':token.input_ids.cuda(),
#                 'token_type_ids':token.token_type_ids.cuda(), 
#                 'attention_mask':token.attention_mask.cuda(),
#                 "visual_feats": image_features,
#                 "visual_pos": visual_pos
#             })
#             outputs = self.model(**inputs)
#             joint_repre = outputs["pooled_output"]
            joint_repre = self.model(image_features, visual_pos, text)
            
        elif self.baseline_name == "vilbert":
#             vis_type=torch.ones(self.args.NUM_OBJECT).long()
#             vis_type_id = vis_type
            # token, feat, bbox, attention_mask=mask,image_attention_mask=vis_mask
#             joint_repre = self.model(token.input_ids.cuda(), image_features, visual_pos, token_mask, vis_mask)
              token_id = token.input_ids.cuda()
#               print(token_id.device,token_mask.device,image_features.device,vis_mask.cuda().device,visual_pos.device)
# self.model(token_id,token_mask,image_features,vis_mask.cuda(),visual_pos)
              joint_repre = self.model(token_id,token_mask,image_features,vis_mask.cuda(),visual_pos)
        
        return joint_repre
