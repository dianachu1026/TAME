'''
Author: Jiawen
Email: dianachu1026@gmail.com
Date: 2021-07-25 23:55:26
LastEditors: Derry
LastEditTime: 2021-08-19 15:57:03
Description: None
'''
import numpy as np
import os
import math
import gc
import gensim.downloader as api
from torch.autograd import Variable
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from transformers import get_linear_schedule_with_warmup,AdamW
from torch.nn.utils.weight_norm import weight_norm
from torch.nn import init
from utils import *

# currently, the semantic model is word2vec mdoel
class Semantic_AutoEncoder(nn.Module):
    def __init__(self):
        super(Semantic_AutoEncoder, self).__init__()

    def forward(self):
        model = api.load('word2vec-google-news-300')  #glove-twitter-200   word2vec-google-news-300
        return model

# Feature Fusion Model
class Gate_Attention(nn.Module):
    def __init__(self,num_hidden_a,num_hidden_b, num_hidden):
        super(Gate_Attention,self).__init__()
        self.hidden=num_hidden
        self.w1 = nn.Parameter(torch.Tensor(num_hidden_a,num_hidden))
        self.w2 = nn.Parameter(torch.Tensor(num_hidden_b,num_hidden))
        self.bias = nn.Parameter(torch.Tensor(num_hidden))
        self.reset_parameter()
        
    def reset_parameter(self):
        stdv1 = 1. / math.sqrt(self.hidden)
        stdv2 = 1. / math.sqrt(self.hidden)
        stdv = (stdv1 + stdv2) / 2.
        self.w1.data.uniform_(-stdv1,stdv1)
        self.w2.data.uniform_(-stdv2,stdv2)
        self.bias.data.uniform_(-stdv,stdv)
        
    def forward(self,a,b):
        wa=torch.matmul(a,self.w1)
        wb=torch.matmul(b,self.w2)
        gated=wa+wb+self.bias
        gate=torch.sigmoid(gated)

        output=gate * a + (1-gate) * b
        return output

# Add Gaussian Noise into X
class GaussianNoiseLayer(nn.Module):
    def __init__(self, mean=0.0, std=0.2):
        super(GaussianNoiseLayer, self).__init__()
        self.mean = mean
        self.std = std

    def forward(self, x):
        if self.training:
            noise = x.data.new(x.size()).normal_(self.mean, self.std)
            if x.is_cuda:
                noise = noise.cuda()
            x = x + noise
        return x

class GANLoss(nn.Module):
    def __init__(self, use_lsgan=True):
        super(GANLoss, self).__init__()
        if use_lsgan:
            self.loss = nn.MSELoss()
        else:
            self.loss = nn.BCELoss()

    def get_target_tensor(self, input, target_is_real):
        # Get soft and noisy labels
        if target_is_real:
            target_tensor = 0.7 + 0.3 * torch.rand(input.size(0))
        else:
            target_tensor = 0.3 * torch.rand(input.size(0))
        if input.is_cuda:
            target_tensor = target_tensor.cuda()
        return target_tensor

    def __call__(self, input, target_is_real):
        target_tensor = self.get_target_tensor(input, target_is_real)
        return self.loss(input.squeeze(), target_tensor)
    
class Encoder(nn.Module):
    def __init__(self, in_dim, hid_dim, out_dim):
        super(Encoder, self).__init__()
        self.linear1 = nn.Linear(in_dim, hid_dim)
        self.linear2 = nn.Linear(hid_dim, out_dim)
        self.relu = nn.Tanh()
        self.enc_mu = nn.Linear(100, 100)
        self.enc_log_sigma = nn.Linear(100, 100)

    def sample_latent(self, h_enc):
        mu = self.enc_mu(h_enc)
        log_sigma = self.enc_log_sigma(h_enc)
        sigma = torch.exp(log_sigma)
        std_z = torch.from_numpy(np.random.normal(0, 1, size=sigma.size())).float().cuda()
        latent_z = mu + sigma * Variable(std_z, requires_grad=False)
        return mu, sigma, latent_z

    def forward(self, x):
        x = self.relu(self.linear1(x))
        x = self.relu(self.linear2(x))
        mu, sigma, z = self.sample_latent(x)
        return mu, sigma, z


# Generator / Decoder
class Generator(nn.Module):
    def __init__(self, in_dim, out_dim, noise=False, use_batchnorm=True, use_dropout=False):
        super(Generator, self).__init__()
        hid_dim = int((in_dim + out_dim) / 2)
        modules = list()
        modules.append(nn.Linear(in_dim, hid_dim))
        if use_batchnorm:
            modules.append(nn.BatchNorm1d(hid_dim))
        modules.append(nn.LeakyReLU(0.2, inplace=True))
        if noise:
            modules.append(GaussianNoiseLayer(mean=0.0, std=0.2))
        if use_dropout:
            modules.append(nn.Dropout(p=0.5))
        modules.append(nn.Linear(hid_dim, hid_dim))
        if use_batchnorm:
            modules.append(nn.BatchNorm1d(hid_dim))
        modules.append(nn.LeakyReLU(0.2, inplace=True))
        if noise:
            modules.append(GaussianNoiseLayer(mean=0.0, std=0.2))
        if use_dropout:
            modules.append(nn.Dropout(p=0.5))
        modules.append(nn.Linear(hid_dim, out_dim))
        self.gen = nn.Sequential(*modules)

    def forward(self, x):
        return self.gen(x)


class Discriminator(nn.Module):
    def __init__(self, in_dim, out_dim, noise=False, use_batchnorm=True, use_dropout=False, use_sigmoid=False):
        super(Discriminator, self).__init__()
        hid_dim = int(in_dim / 2)
        modules = list()
        if noise:
            modules.append(GaussianNoiseLayer(mean=0.0, std=0.3))
        modules.append(nn.Linear(in_dim, hid_dim))
        if use_batchnorm:
            modules.append(nn.BatchNorm1d(hid_dim))
        modules.append(nn.LeakyReLU(0.2, inplace=True))
        if use_dropout:
            modules.append(nn.Dropout(p=0.5))
        modules.append(nn.Linear(hid_dim, hid_dim))
        if use_batchnorm:
            modules.append(nn.BatchNorm1d(hid_dim))
        modules.append(nn.LeakyReLU(0.2, inplace=True))
        if use_dropout:
            modules.append(nn.Dropout(p=0.5))
        modules.append(nn.Linear(hid_dim, out_dim))
        if use_sigmoid:
            modules.append(nn.Sigmoid())
        self.disc = nn.Sequential(*modules)

    def forward(self, x):
        return self.disc(x)

class SingleClassifier(nn.Module):
    def __init__(self, in_dim, out_dim, dropout):
        super(SingleClassifier, self).__init__()
        layer=[
            weight_norm(nn.Linear(in_dim, out_dim),dim=None),
            nn.Dropout(dropout,inplace=True)
        ]
        self.main=nn.Sequential(*layer)
        
    def forward(self, x):
        logits=self.main(x)
        return logits
    
class SimpleClassifier(nn.Module):
    def __init__(self, in_dim, hid_dim, out_dim, dropout):
        super(SimpleClassifier, self).__init__()
        layer=[
            weight_norm(nn.Linear(in_dim, hid_dim),dim=None),
            nn.ReLU(),
            nn.Dropout(dropout, inplace=True),
            weight_norm(nn.Linear(hid_dim, out_dim),dim=None)
        ]
        self.main=nn.Sequential(*layer)
        
    def forward(self,x):
        logits=self.main(x)
        return logits

class Classifier(nn.Module):
    def __init__(self, in_dim, hid_dim1, hid_dim2, out_dim, dropout):
        super(Classifier, self).__init__()
        self.layer_1 = nn.Linear(in_dim, hid_dim1)
        self.layer_2 = nn.Linear(hid_dim1, hid_dim2)
        self.layer_out = nn.Linear(hid_dim2, out_dim)
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(p=dropout)
        self.batchnorm1 = nn.BatchNorm1d(hid_dim1)
        self.batchnorm2 = nn.BatchNorm1d(hid_dim2)

    def forward(self, inputs):
        x = self.relu(self.layer_1(inputs))
        x = self.batchnorm1(x)
        x = self.relu(self.layer_2(x))
        x = self.batchnorm2(x)
        x = self.dropout(x)
        x = self.layer_out(x)
        return x
    
class VAEGAN_hmm(nn.Module):
    def __init__(self, args, vis_enc, transformer):
        super(VAEGAN_hmm, self).__init__()
        print('Initializing trainable models...', end='')
        self.vis_enc = vis_enc
        self.feature_extractor = FeatureExtractor(args, vis_enc, transformer)
        self.encoder = Encoder(768, 256, 100)    #300, 256, 128
        self.generator = Generator(1124, 768, use_batchnorm=True, use_dropout=True)  #868, 300  1124
        self.discriminator = Discriminator(768, 1, noise=True, use_batchnorm=True)  #300, 1
#         self.gate = Gate_Attention(768, 768, 768)
        self.classifier = Classifier(768, 256, 128, 2, 0.1)    #1068, 512, 256, 2, 0.1
        self.classifier_sem = nn.Linear(768, 6, bias=False)   #300, 6
        for param in self.classifier_sem.parameters():
            param.requires_grad = False
        print("Done")

        print('Defining optimizers...', end='')
        self.lr = args.lr
        self.gamma = args.gamma
        self.momentum = args.momentum
        self.milestones = args.milestones
        self.batch_size = args.batch_size
        self.optimizer_gen = torch.optim.AdamW(
            list(self.vis_enc.parameters())+
            list(self.generator.parameters())+
            list(self.encoder.parameters())+
            list(self.classifier.parameters()), lr = self.lr, eps = 1e-8)
        self.optimizer_disc = optim.SGD(list(self.discriminator.parameters()), lr=self.lr, momentum=self.momentum)
        self.scheduler_gen = get_linear_schedule_with_warmup(self.optimizer_gen, 
                                                num_warmup_steps = 0, 
                                                num_training_steps = args.total_steps)
        self.scheduler_disc = optim.lr_scheduler.MultiStepLR(self.optimizer_disc, milestones=self.milestones,
                                                             gamma=self.gamma)
        print('Done')

        print('Defining losses...', end='')
        self.lambda_im = args.lambda_im
        self.lambda_se = args.lambda_se
        self.lambda_adv = args.lambda_adv
        self.lambda_cyc = args.lambda_cyc
        self.lambda_dis = args.lambda_dis
        self.lambda_cls = args.lambda_cls
        self.lambda_cls_sem = args.lambda_cls_sem
        
        weight = torch.tensor([0.5, 0.7]).cuda()   # Weighted cls loss function
        self.criterion_cls1 = nn.CrossEntropyLoss(weight=weight)

        self.criterion_cls = nn.CrossEntropyLoss()
        self.criterion_cyc = nn.L1Loss()
        self.criterion_gan = GANLoss(use_lsgan=True)
        print('Done')

        # Intermediate variables
        print('Initializing variables...', end='')
        self.x = torch.zeros(1)
        self.z = torch.zeros(1)
        self.s = torch.zeros(1)
        self.x_hat = torch.zeros(1)
        self.s_hat1 = torch.zeros(1)
        self.s_hat2 = torch.zeros(1)
        self.pred = torch.zeros(1)
        self.prob = torch.zeros(1)   
        print('Done')

    def forward(self, text, image, cls_features, label=None):
        self.x = self.feature_extractor(text, image)
        self.s = cls_features
#         joint_pre = torch.cat((self.x, self.s), dim = 1)
        self.mu, self.sigma, self.z = self.encoder(self.s)
        noise = torch.normal(0, 1, size=self.z.size()).cuda()
        
        x_z = torch.cat((self.z, self.x), dim = 1)
        x_noise = torch.cat((noise, self.x), dim = 1)
        
        self.s_hat = self.generator(x_z)
        self.s_hat2 = self.generator(x_noise)
        
#         joint_pre = torch.cat((self.x, self.s_hat2), dim = 1)
#         joint_pre = self.gate(self.x, self.s_hat2)
        self.pred = self.classifier(self.s_hat2)
        self.prob = F.softmax(self.pred, dim = 1)
        

    def backward(self, sem_label, label):
        # KL loss
        kl_loss = -0.5 * torch.sum(1 + self.sigma - self.mu.pow(2) - self.sigma.exp())
        
#         adv loss
        a1 = self.discriminator(self.s_hat)   
        a2 = self.discriminator(self.s_hat2)
        loss_adv =  self.criterion_gan(a1, True) + self.criterion_gan(a2, True)
        loss_adv = self.lambda_adv * loss_adv
        
        # cyc loss  + self.lambda_se * self.criterion_cyc(self.s, self.s_hat2)
        loss_cyc = self.lambda_im * self.criterion_cyc(self.s, self.s_hat) 
        loss_cyc = self.lambda_cyc* loss_cyc
        
#         # cls loss
        loss_cls = self.lambda_cls * self.criterion_cls(self.prob, label)
        
#         !!!!! semantic cls loss    self.criterion_cls(self.classifier_sem(self.s_hat), sem_label) +
        loss_cls_sem = self.lambda_cls_sem * (self.criterion_cls(self.classifier_sem(self.s_hat), sem_label)+
                                              self.criterion_cls(self.classifier_sem(self.s_hat2), sem_label))
        
         # Sum the above generator losses for back propagation and displaying   + loss_cls_sem  kl_loss + 
        loss_gen = loss_adv + loss_cls + loss_cls_sem

        self.optimizer_gen.zero_grad()
        loss_gen.backward(retain_graph=True)
        self.optimizer_gen.step()

        # initialize optimizer
        self.optimizer_disc.zero_grad()

        # Semantic discriminator loss  self.criterion_gan(a1.detach(), False) + 
        a1 = self.discriminator(self.s_hat) 
        a2 = self.discriminator(self.s_hat2)
        loss_disc = self.criterion_gan(self.discriminator(self.s), True) + self.criterion_gan(a1.detach(), False) + self.criterion_gan(a2.detach(), False)
        loss_disc = self.lambda_dis * loss_disc
        
        loss_disc.backward(retain_graph=True)
        self.optimizer_disc.step()

        loss = loss_adv

        return loss

    def optimize_params(self, text, image, sem_feature, sem_label, label):
#         print(label)
        self.forward(text, image, sem_feature, label)
        loss = self.backward(sem_label, label)
        return loss, self.prob
    
    def get_prediction(self, text, image):
        x = self.feature_extractor(text, image)
        
        noise = torch.normal(0, 1, size=[x.size(0), 100]).cuda()
        x_noise = torch.cat((noise, x), dim = 1)
        s_hat = self.generator(x_noise)
#         joint_pre = torch.cat((x, s_hat), dim = 1)
        
#         joint_pre = self.gate(x, s_hat)
        
        pred = self.classifier(s_hat)
        prob = F.softmax(pred, dim = 1)
        return prob
    
    def get_sem(self, text, image):
        x = self.feature_extractor(text, image)
        noise = torch.normal(0, 1, size=[x.size(0), 100]).cuda()
        x_noise = torch.cat((noise, x), dim = 1)
        s_hat = self.generator(x_noise)
        
        return s_hat
    
    def get_sample_prediction(self, text, image):
        x = self.feature_extractor(text, image)
        noise = torch.normal(0, 1, size=[x.size(0), 100]).cuda()
        x_noise = torch.cat((noise, x), dim = 1)
        s_hat = self.generator(x_noise)
        
        pred_hate = self.classifier(s_hat)
        prob_hate = F.softmax(pred_hate, dim = 1)
        
        return prob_hate


    
    
# class VAEGAN_hmm(torch.nn.Module):
#     def __init__(self, args):
#         super(VAEGAN_hmm, self).__init__()
#         print('Initializing trainable models...', end='')
#         self.encoder = Encoder(1068, 512, 300)
#         self.generator = Generator(400, 768, noise=True, use_batchnorm=True, use_dropout=True)
#         self.discriminator = Discriminator(768, 1, noise=True, use_batchnorm=True, use_dropout=True)
#         self.decoder = Generator(768, 300)
#         self.classifier = Classifier(1068, 512, 256, 2, 0.1)
#         self.classifier_sem = nn.Linear(300, 6, bias=False)
#         for param in self.classifier_sem.parameters():
#             param.requires_grad = False
#         print("Done")

#         # Optimizers
#         print('Defining optimizers...', end='')
#         self.lr = args.lr
#         self.gamma = args.gamma
#         self.momentum = args.momentum
#         self.milestones = args.milestones
#         self.optimizer_gen = torch.optim.AdamW(
#             list(self.generator.parameters())+
#             list(self.encoder.parameters())+
#             list(self.decoder.parameters())+
#             list(self.classifier.parameters()), lr=self.lr, eps = 1e-8)
#         self.optimizer_disc = optim.SGD(list(self.discriminator.parameters()), lr=self.lr, momentum=self.momentum)
# #         self.scheduler_gen = optim.lr_scheduler.MultiStepLR(self.optimizer_gen, milestones=self.milestones,
# #                                                             gamma=self.gamma)
#         self.scheduler_gen = get_linear_schedule_with_warmup(self.optimizer_gen, 
#                                                 num_warmup_steps = 0, 
#                                                 num_training_steps = args.total_steps)
# #         print("total step: ", args.total_steps)
#         self.scheduler_disc = optim.lr_scheduler.MultiStepLR(self.optimizer_disc, milestones=self.milestones,
#                                                              gamma=self.gamma)
#         print('Done')

#         # Loss function
#         print('Defining losses...', end='')
#         self.lambda_adv = args.lambda_adv
#         self.lambda_cyc = args.lambda_cyc
#         self.lambda_dis = args.lambda_dis
#         self.lambda_cls = args.lambda_cls
#         self.lambda_im = args.lambda_im
#         self.lambda_se = args.lambda_se
#         self.lambda_cls_sem = args.lambda_cls_sem
        
        
#         self.criterion_cyc = nn.L1Loss()
# #         weight = torch.tensor([0.3, 0.7]).cuda()
# #         self.criterion_cls = nn.CrossEntropyLoss(weight=weight)
#         self.criterion_cls = nn.CrossEntropyLoss()
#         self.criterion_reg = nn.MSELoss()
#         self.criterion_gan = GANLoss(use_lsgan=True)
#         print('Done')

#         # Intermediate variables
#         print('Initializing variables...', end='')
#         self.x = torch.zeros(1)
#         self.z = torch.zeros(1)
#         self.s = torch.zeros(1)
#         self.x_hat = torch.zeros(1)
#         self.s_hat1 = torch.zeros(1)
#         self.s_hat2 = torch.zeros(1)
#         self.pred = torch.zeros(1)
#         self.prob = torch.zeros(1)                                                   
#         print('Done')

#     def forward(self, hmm_feature, cls_features, label=None):
#         self.x = hmm_feature
#         self.s = cls_features
#         joint_pre = torch.cat((self.x, self.s), dim = 1)
#         self.mu, self.sigma, self.z = self.encoder(joint_pre)
#         joint_pre2 = torch.cat((self.z, self.s), dim = 1)
#         self.x_hat = self.generator(joint_pre2)
#         self.s_hat = self.decoder(self.x)
#         self.s_hat2 = self.decoder(self.x_hat)
        
#         # add multimodel features
#         joint_pre3 = torch.cat((self.x, self.s_hat), dim = 1)
#         self.pred = self.classifier(joint_pre3)
#         self.prob = F.softmax(self.pred, dim = 1)

#     def backward(self, sem_label, label):
#         # KL loss
#         kl_loss = -0.5 * torch.sum(1 + self.sigma - self.mu.pow(2) - self.sigma.exp())
        
#         # adv loss
#         a1 = self.discriminator(self.x_hat)
#         loss_adv = self.criterion_gan(a1, True)
#         loss_adv = self.lambda_adv * loss_adv
        
#         # cyc loss
# #         loss_cyc = self.lambda_im * self.criterion_cyc(self.x, self.x_hat) + self.lambda_se * self.criterion_cyc(self.s, self.s_hat2) + self.criterion_cyc(self.s_hat, self.s_hat2)
#         loss_cyc = self.lambda_im * self.criterion_cyc(self.x, self.x_hat) + self.lambda_se * self.criterion_cyc(self.s, self.s_hat2)
#         loss_cyc = self.lambda_cyc* loss_cyc
        
#         # cls loss
#         loss_cls = self.lambda_cls * self.criterion_cls(self.prob, label)

# #         !!!!! semantic cls loss
#         loss_cls_sem = self.lambda_cls_sem * (self.criterion_cls(self.classifier_sem(self.s_hat), sem_label) +
#                                          self.criterion_cls(self.classifier_sem(self.s_hat2), sem_label))
        
# #          Sum the above generator losses for back propagation and displaying   + loss_cls_sem  kl_loss + 
#         loss_gen = kl_loss + loss_adv + loss_cyc + loss_cls + loss_cls_sem
        
# #          # Sum the above generator losses for back propagation and displaying
# #         loss_gen = kl_loss + loss_adv + loss_cyc + loss_cls

#         self.optimizer_gen.zero_grad()
#         loss_gen.backward(retain_graph=True)
#         self.optimizer_gen.step()

#         # initialize optimizer
#         self.optimizer_disc.zero_grad()

#         # Semantic discriminator loss
#         a1 = self.discriminator(self.x_hat)
#         loss_disc = self.criterion_gan(self.discriminator(self.x), True) + \
#                        self.criterion_gan(a1.detach(), False)
#         loss_disc = self.lambda_dis * loss_disc
        
#         loss_disc.backward(retain_graph=True)
#         self.optimizer_disc.step()

#         loss = {'kl_loss': kl_loss, 'loss_adv': loss_adv, 'loss_cyc': loss_cyc, 'loss_cls': loss_cls,'loss_disc': loss_disc}

#         return loss, self.prob

#     def optimize_params(self, hmm_feature, sem_feature, sem_label, label):
#         self.forward(hmm_feature, sem_feature, label)
#         prob = self.backward(sem_label, label)
#         return prob
    
#     def get_prediction(self, x):
#         s_hat = self.decoder(x)
#         joint_pre = torch.cat((x, s_hat), dim = 1)
#         pred = self.classifier(joint_pre)
#         prob = F.softmax(pred, dim = 1)
#         return prob
    
#     def get_sem(self, x):
#         s_hat = self.decoder(x)
#         return s_hat
    
#     def get_x_hat(self, hmm_feature, cls_features):
#         x = hmm_feature
#         s = cls_features
#         joint_pre = torch.cat((x, s),dim = 1)
#         mu, sigma, z = self.encoder(joint_pre)
#         joint_pre2 = torch.cat((z, s),dim = 1)
#         x_hat = self.generator(joint_pre2)
#         return x_hat
    