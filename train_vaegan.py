import os
import torch
import torch.nn.functional as F
from sklearn.metrics import roc_auc_score, accuracy_score
from transformers import BertModel
import config
from model import *
from utils import *
from test import *
import pickle

# torch.cuda.is_available()
torch.cuda.set_device(0)
text_transformer = BertTokenizer.from_pretrained("bert-base-uncased")
entity_file = open("../hateful_memes/data/entity_dict.pkl",'rb')
entity_info = pickle.load(entity_file)
race_file = open("../hateful_memes/data/race_dict.pkl",'rb')
race_info = pickle.load(race_file)

def train(model, train_set, gzsl_test_frame, cla_name, vis_enc, text_transformer):
    model.train()
    eval_acc_list = []
    eval_auroc_list = []
    word2vec = Semantic_AutoEncoder()
#     wv = word2vec.forward()
    wv = BertModel.from_pretrained("bert-base-uncased")
    d = {'non_hateful': 0, 'nationality': 1, 'disability': 2, 'race': 3, 'religion': 4, 'sex': 5}
    for epo in range(1, args.epoch + 1):
#         print(1)
        pred_list = []
        label_list = []
        correct_sum = 0
        all_sum = 0
        for data in train_set:
#             print(2)
            image, text, clas, label = data
            label = label.cuda()
            clas = list(clas)
            texts = sorted(list(set(clas)))
            sem_label = np.array([d.get(t) for t in clas])
            sem_label = torch.from_numpy(sem_label).cuda()
            
            # extra
            ext_text = []
            ext_clas = []
            for i in range(len(image)):
                item = image[i].split('/')[1]
                entity_list = get_list(entity_info[item])
                race_list = extract_list(race_info[item])
                text_list = text[i]
                cls_list = clas[i]
                ext_txt = text_list + ' [SEP] '+' '.join(entity_list)+' [SEP] '+' '.join(race_list)
                ext_cla = cls_list + ' [SEP] '+' '.join(entity_list)+' [SEP] '+' '.join(race_list)
                ext_text.append(ext_txt)
                ext_clas.append(ext_cla)
            
            for i in range(len(clas)):
                if clas[i] == "non_hateful":
                    clas[i] = "non"
                    
            # extra
            encoded_input = text_transformer(
                ext_clas,
                max_length=64,
                padding=True,
                truncation=True,
                return_tensors="pt")
            sem_feature = wv(**encoded_input).pooler_output
#             print(sem_feature)
#             print(len(sem_feature))
#             print(len(sem_feature[0]))

#             sem_feature = wv[clas]
            sem_feature = torch.Tensor(sem_feature).cuda()
            if args.mode == "EXT":
                loss, prob = model.optimize_params(ext_text, image, sem_feature, sem_label, label)
            else:
                loss, prob = model.optimize_params(text, image, sem_feature, sem_label, label)
                
            ans, _ = pred_analys(prob)
            label = label.cpu()
            label = np.array(label)
            ans = np.array(ans)
            correct_results_sum = sum(ans == label)
            correct_sum = correct_sum + correct_results_sum
            all_sum = all_sum + len(label)
        acc = correct_sum / all_sum
        print("EPOCH ", epo)
        print("accuracy: ", acc)
        name = "model" + str(epo)+".pth"
        torch.save(model.state_dict(), args.output_path + name)
        eval_acc, eval_auroc = test(model, gzsl_test_frame, args, vis_enc, text_transformer)
        eval_acc_list.append(eval_acc)
        eval_auroc_list.append(eval_auroc)
    max_acc = max(eval_acc_list)
    max_auroc = max(eval_auroc_list)
    print("Max accuracy: ", max_acc, " Max auroc: ", max_auroc)
            
    print("training finished!")
                

if __name__ == "__main__":
    args = config.parse_opt()
    setup_seed(args.seed)
    my_list = ["religion", "sex", "race", "disability", "nationality"]
#     my_list = ["religion"]
    for i in my_list:
        item = i
        args.date = "1113"
        args.output_path = "./model-outputs/prop_vilbert/" + args.date + "_"+ item + "/"
        vis_enc = get_model(args, args.baseline_name)
        train_frame, gzsl_test_frame, zsl_test_frame, hate_test_frame = load_data(item, args)
        
        if item == "disability" or item == "nationality":
            fi_hate = hate_test_frame
        else:
            fi_hate = hate_test_frame[0:254]
            
        print(len(fi_hate))
        print(len(zsl_test_frame))
        
        zsl_test_frame = zsl_test_frame.append(fi_hate)

        train_loader = prepare_data(train_frame, args)
        zsl_test_loader = prepare_data(zsl_test_frame, args)
        hate_test_loader = prepare_data(hate_test_frame, args)

        total_steps=len(train_loader)*args.epoch
        args.total_steps = total_steps

        model = VAEGAN_hmm(args, vis_enc, text_transformer)
        model = model.cuda()

        print("Training start: ", item)
        train(model, train_loader, gzsl_test_frame, item, vis_enc, text_transformer)
        print("ZSL accuracy for " + item + " :")
        hate_test(model, zsl_test_loader, args, vis_enc, text_transformer)





