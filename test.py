import os
import torch
import torch.nn.functional as F
from sklearn.metrics import roc_auc_score, accuracy_score
from model import *
from utils import *

def pred_analys(pred):
    ans = []
    pred_true = []
    pred_true = []
    for t in pred:
        pred_true.append(t[1])
        if t[0]>t[1]:
            ans.append(0)
        else:
            ans.append(1)
    ans = torch.Tensor(ans)
    return ans, pred_true

def hate_test(model, test_set, args, vis_enc, transformer, test_mode = None):
    model.eval()
    if test_mode == "hate":
        pred_list = []
        label_list = []
        with torch.no_grad():
            for data in test_set:
                image, text, clas, label = data
                label = label.cuda()
#                 feature_extractor = FeatureExtractor(args, vis_enc, transformer)
#                 hmm_feature = feature_extractor(text, image)
#                 prob = model.forward(hmm_feature)
                prob = model.get_prediction(text, image)
                pred_list.extend(prob)
                label_list.extend(label)
#         pred = F.softmax(torch.stack(pred_list), dim = 1)
        pred = torch.stack(pred_list)
        ans, _ = pred_analys(pred)
        label_list = torch.Tensor(label_list).cpu()
        acc = accuracy_score(ans, label_list)
        return acc
        
    else:
        for i in range(1, args.epoch + 1):
            pred_list = []
            label_list = []
            file = args.output_path
            model_name = "model"+str(i)+".pth"
            model.load_state_dict(torch.load(file + model_name))
            with torch.no_grad():
                for data in test_set:
                    image, text, clas, label = data
                    label = label.cuda()
#                     feature_extractor = FeatureExtractor(args, vis_enc, transformer)
#                     hmm_feature = feature_extractor(text, image)
#                     prob = model.forward(hmm_feature)
                    prob = model.get_prediction(text, image)
                    pred_list.extend(prob)
                    label_list.extend(label)
#             pred = F.softmax(torch.stack(pred_list), dim = 1)
            pred = torch.stack(pred_list)
    
            ans, pred_true = pred_analys(pred)
            pred_true = torch.Tensor(pred_true)

            label_list = torch.Tensor(label_list).cpu()
            acc = accuracy_score(ans, label_list)
            auroc = roc_auc_score(label_list, pred_true)
            print("model" + str(i), " accuracy= {:.4f}".format(acc), " auroc= {:.4f}".format(auroc))

def test(model, test_data, args, vis_enc, transformer):
    model.eval()
    pred_list = []
    label_list = []
    test_set = prepare_data(test_data, args)
        
    with torch.no_grad():
        for data in test_set:
            image, text, clas, label = data
            label = label.cuda()
#             feature_extractor = FeatureExtractor(args, vis_enc, transformer)
#             hmm_feature = feature_extractor(text, image)
#             prob = model.forward(hmm_feature)  
            prob = model.get_prediction(text, image)
            pred_list.extend(prob)
            label_list.extend(label)
            
#     pred = F.softmax(torch.stack(pred_list), dim = 1)  # pred = torch.stack(pred_list)
#     pred = F.softmax(torch.stack(pred_list), dim = 1)
    pred = torch.stack(pred_list)
    ans, pred_true = pred_analys(pred)
    pred_true = torch.Tensor(pred_true)
        
    label_list = torch.Tensor(label_list).cpu()
    acc = accuracy_score(ans, label_list)
    auroc = roc_auc_score(label_list, pred_true)
    print("Eval accuracy: ", acc, "auc: ", auroc)
        
    cls_list = ["race", "nationality", "disability", "religion", "sex"]
    for item in cls_list:
        cls_frame = test_data[test_data["cls"] == item]
        cls_test = prepare_data(cls_frame, args)
        hate_acc = hate_test(model, cls_test, args, vis_enc, transformer, test_mode = "hate")
        print("Eval accuracy for ", item, ": ", hate_acc)
    return acc, auroc

def test_feature_base(model, test_data, args, vis_enc, transformer):
    model.eval()
    base_feature_list = []
    vg_feature_list = []
    label_list = []
    file = args.output_path
    model_name = "model"+str(7)+".pth"
    model.load_state_dict(torch.load(file + model_name))
    d = {'non_hateful': 0, 'nationality': 1, 'disability': 2, 'race': 3, 'religion': 4, 'sex': 5}
    with torch.no_grad():
        for data in test_data:
            image, text, clas, label = data
            label = label.cuda()
            
            texts = sorted(list(set(clas)))
            sem_label = np.array([d.get(t) for t in clas])
            sem_label = torch.from_numpy(sem_label).cuda()
            
            feature_extractor = FeatureExtractor(args, vis_enc, transformer)
            hmm_feature = feature_extractor(text, image)
            vg_feature = model.get_sem(text, image)
            base_feature_list.extend(hmm_feature)
            vg_feature_list.extend(vg_feature)
            label_list.extend(sem_label)
    base_feature_list = torch.stack(base_feature_list).cpu().tolist()
    vg_feature_list = torch.stack(vg_feature_list).cpu().tolist()
    label_list = torch.stack(label_list).cpu().tolist()
    return base_feature_list, vg_feature_list, label_list

def test_samples(model, test_data, args, vis_enc, transformer):
    model.eval()
    prob_hate_list = []
    prob_sem_list = []
    label_list = []
    sem_list = []
    image_list = []
    file = args.output_path
    model_name = "model"+str(14)+".pth"
    model.load_state_dict(torch.load(file + model_name))
    
    with torch.no_grad():
        for data in test_data:
            image, text, clas, label = data
            label = label.cuda()
            texts = sorted(list(set(clas)))
            feature_extractor = FeatureExtractor(args, vis_enc, transformer)
            hmm_feature = feature_extractor(text, image)
            prob_hate = model.forward(hmm_feature)
            
#             prob_hate = model.get_sample_prediction(hmm_feature)
            prob_hate_list.extend(prob_hate)
            label_list.extend(label)
            image_list.extend(image)
            
    prob_hate_list = torch.stack(prob_hate_list).cpu().tolist()
    label_list = torch.stack(label_list).cpu().tolist()
    ans_hate_list, _ = pred_analys(prob_hate_list)

    return ans_hate_list, label_list, image_list