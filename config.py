'''
Author: Jiawen
Email: dianachu1026@gmail.com
Date: 2021-07-25 23:55:26
LastEditors: Jiawen
LastEditTime: 2021-07-26 18:06:55
Description: None
'''
import argparse

def parse_opt():
    parser=argparse.ArgumentParser()  
    parser.add_argument('--LENGTH',type=int,default=64)
    #vil-bert related parameters
    parser.add_argument("--PRE_BERT_TYPE",type=str,
                        default='bert-base-uncased')
    parser.add_argument(
        "--BERT_CONFIG",
        type=str,
        default='./model-outputs/vilbert/bert_base_6layer_6conect.json')
    
    #lxmert related parameters
    parser.add_argument("--train", default='train')
    parser.add_argument("--valid", default='valid')
    parser.add_argument("--test", default=None)

    # Training Hyper-parameters
    parser.add_argument('--optim', default='bert')
    parser.add_argument('--lr', type=float, default=2e-5)
    parser.add_argument('--dropout', type=float, default=0.1)
    parser.add_argument('--seed', type=int, default=128, help='random seed')

    # Debugging
    parser.add_argument("--fast", action='store_const', default=False, const=True)
    parser.add_argument("--tiny", action='store_const', default=False, const=True)
    parser.add_argument("--tqdm", action='store_const', default=False, const=True)
    parser.add_argument('--NUM_OBJECT',type=int,default=36)
    parser.add_argument('--loadLXMERTQA', dest='load_lxmert_qa', type=str, default='./model-outputs/lxmert/model',
                        help='Load the pre-trained LXMERT model with QA answer head.')

    # Model Loading
    parser.add_argument('--load', type=str, default=None,
                        help='Load the model (usually the fine-tuned model).')
    parser.add_argument('--loadLXMERT', dest='load_lxmert', type=str, default=None,
                        help='Load the pre-trained LXMERT model.')
    parser.add_argument("--fromScratch", dest='from_scratch', action='store_const', 
                        default=False, const=True,
                        help='If none of the --load, --loadLXMERT, --loadLXMERTQA is set, '
                             'the model would be trained from scratch. If --fromScratch is'
                             ' not specified, the model would load BERT-pre-trained weights by'
                        ' default. ')

    # Optimization
    parser.add_argument("--mceLoss", dest='mce_loss', action='store_const', default=False, const=True)

    # LXRT Model Config
    # Note: LXRT = L, X, R (three encoders), Transformer
    parser.add_argument("--llayers", default=9, type=int, help='Number of Language layers')
    parser.add_argument("--xlayers", default=5, type=int, help='Number of CROSS-modality layers.')
    parser.add_argument("--rlayers", default=5, type=int, help='Number of object Relationship layers.')

    # LXMERT Pre-training Config
    parser.add_argument("--taskMatched", dest='task_matched', action='store_const', default=False, const=True)
    parser.add_argument("--taskMaskLM", dest='task_mask_lm', action='store_const', default=False, const=True)
    parser.add_argument("--taskObjPredict", dest='task_obj_predict', action='store_const', default=False, const=True)
    parser.add_argument("--taskQA", dest='task_qa', action='store_const', default=False, const=True)
    parser.add_argument("--visualLosses", dest='visual_losses', default='obj,attr,feat', type=str)
    parser.add_argument("--qaSets", dest='qa_sets', default=None, type=str)
    parser.add_argument("--wordMaskRate", dest='word_mask_rate', default=0.15, type=float)
    parser.add_argument("--objMaskRate", dest='obj_mask_rate', default=0.15, type=float)

    # Training configuration
    parser.add_argument("--multiGPU", action='store_const', default=False, const=True)
    parser.add_argument("--numWorkers", dest='num_workers', default=0)
    
    parser.add_argument('--gamma', type=float, default=0.1)
    parser.add_argument('--momentum', type=float, default=0.9)
    parser.add_argument('--weight_decay', type=float, default=5e-4)
    parser.add_argument('--epoch', type=int, default=20)
    parser.add_argument('--batch_size', type=int, default=64)
    parser.add_argument('--data_dir', type=str, default="/opt/jwzhu/hateful_memes")
    parser.add_argument('--date', type=str, default="test")
    parser.add_argument('--output_path', type=str, default="./model-outputs/")
    parser.add_argument('--baseline_name', type=str, default="vilbert")
    parser.add_argument('--mode', type=str, default="Normal")
    
    parser.add_argument('--lambda-se', default=10.0, type=float, help='Weight on the semantic model')
    parser.add_argument('--lambda-im', default=10.0, type=float, help='Weight on the image model')
    parser.add_argument('--lambda_cyc', default=1.0, type=float, help='Weight on cycle consistency loss (gen)')
    parser.add_argument('--lambda_adv', default=1.0, type=float, help='Weight on adversarial loss (gen)')
    parser.add_argument('--lambda_cls', default=5.0, type=float, help='Weight on classification loss (gen)')
    parser.add_argument('--lambda_cls_sem', default=5.0, type=float, help='Weight on semantic classification loss (gen)')
    parser.add_argument('--lambda_dis', default=1.0, type=float, help='Weight on discriminator loss (disc)')
    parser.add_argument('--milestones', type=int, nargs='+', default=[], help='Milestones for scheduler')
    parser.add_argument('--total_steps', type=int, default=100, help='total training step')
    
    args=parser.parse_args()
    return args



# def parse_opt():
#     # Training settings
#     parser = argparse.ArgumentParser()
#     # parser.add_argument('--no_cuda', action='store_true', default=False)
#     # parser.add_argument('--fastmode', action='store_true', default=True)
#     parser.add_argument('--seed', type=int, default=128, help='Random seed.')
#     parser.add_argument('--lr', type=float, default=0.0001)
#     parser.add_argument('--gamma', type=float, default=0.1)
#     parser.add_argument('--momentum', type=float, default=0.9)
#     parser.add_argument('--weight_decay', type=float, default=5e-4)
#     parser.add_argument('--epoch', type=int, default=20)
#     parser.add_argument('--batch_size', type=int, default=128)
#     parser.add_argument('--data_dir', type=str, default="/opt/jwzhu/hateful_memes")
#     parser.add_argument('--date', type=str, default="test")
#     parser.add_argument('--output_path', type=str, default="./model-outputs/")
#     parser.add_argument('--baseline_name', type=str, default="visualbert")
    
#     parser.add_argument('--lambda-se', default=10.0, type=float, help='Weight on the semantic model')
#     parser.add_argument('--lambda-im', default=10.0, type=float, help='Weight on the image model')
#     parser.add_argument('--lambda_cyc', default=1.0, type=float, help='Weight on cycle consistency loss (gen)')
#     parser.add_argument('--lambda_adv', default=1.0, type=float, help='Weight on adversarial loss (gen)')
#     parser.add_argument('--lambda_cls', default=5.0, type=float, help='Weight on classification loss (gen)')
#     parser.add_argument('--lambda_cls_sem', default=5.0, type=float, help='Weight on semantic classification loss (gen)')
#     parser.add_argument('--lambda_dis', default=1.0, type=float, help='Weight on discriminator loss (disc)')
#     parser.add_argument('--milestones', type=int, nargs='+', default=[], help='Milestones for scheduler')
#     parser.add_argument('--total_steps', type=int, default=100, help='total training step')

#     args = parser.parse_args()
#     return args